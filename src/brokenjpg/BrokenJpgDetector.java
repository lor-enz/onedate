package brokenjpg;

import misc.bl;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

public class BrokenJpgDetector {


    public static boolean isJpgBroken(final File file) {
        try {
            return checkJpgValidity(file.toPath()).isBroken();
        } catch (NoSuchAlgorithmException | IOException e) {
            bl.log.severe("Error in isJpgBroken: " + e.getCause() + "->" + e.getMessage());
            return true;
        }
    }

    public static JpgAnalysisResult checkJpgValidity(final File file) {
        try {
            ImageAnalysisResult result = checkJpgValidity(file.toPath());
            if (!result.truncated && result.image)
                return JpgAnalysisResult.VALID;
            else if (!result.truncated && !result.image)
                return JpgAnalysisResult.NON_IMAGE;
            else if (result.truncated && !result.image)
                return JpgAnalysisResult.NON_IMAGE_AND_TRUNCATED;
            else if (result.truncated && result.image)
                return JpgAnalysisResult.TRUNCATED;

        } catch (NoSuchAlgorithmException | IOException e) {
            bl.log.severe("Error in isJpgBroken: " + e.getCause() + "->" + e.getMessage());
        }
        return JpgAnalysisResult.UNKNOWN_ERROR;
    }

    /**
     * Copied straight from:
     * https://stackoverflow.com/questions/8039444/how-to-detect-corrupted-images-png-jpg-in-java
     *
     * @param file Probably just use new File("blabla").getPath() for this
     * @return Probably just use .isBroken() which returns boolean. or use the two bools to get more details on whats broken
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    private static ImageAnalysisResult checkJpgValidity(final Path file)
            throws NoSuchAlgorithmException, IOException {
        final ImageAnalysisResult result = new ImageAnalysisResult();

        final InputStream digestInputStream = Files.newInputStream(file);
        try {
            final ImageInputStream imageInputStream = ImageIO
                    .createImageInputStream(digestInputStream);
            final Iterator<ImageReader> imageReaders = ImageIO
                    .getImageReaders(imageInputStream);
            if (!imageReaders.hasNext()) {
                result.setImage(false);
                return result;
            }
            final ImageReader imageReader = imageReaders.next();
            imageReader.setInput(imageInputStream);
            final BufferedImage image = imageReader.read(0);
            if (image == null) {
                return result;
            }
            image.flush();
            if (imageReader.getFormatName().equals("JPEG")) {
                imageInputStream.seek(imageInputStream.getStreamPosition() - 2);
                final byte[] lastTwoBytes = new byte[2];
                imageInputStream.read(lastTwoBytes);
                if (lastTwoBytes[0] != (byte) 0xff || lastTwoBytes[1] != (byte) 0xd9) {
                    result.setTruncated(true);
                } else {
                    result.setTruncated(false);
                }
            }
            result.setImage(true);
        } catch (final IndexOutOfBoundsException e) {
            result.setTruncated(true);
        } catch (final IIOException e) {
            if (e.getCause() instanceof EOFException) {
                result.setTruncated(true);
            }
        } finally {
            digestInputStream.close();
        }
        return result;
    }

}

