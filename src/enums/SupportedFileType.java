package enums;


public enum SupportedFileType {

    BMP(new String[]{"bmp"}, false),
    PNG(new String[]{"png"}, false),
    JPG(new String[]{"jpg","jpeg"}, false),
    GIF(new String[]{"gif"}, false),
    MP4(new String[]{"mp4"}, true),
    MOV(new String[]{"mov"}, true),
    AVI(new String[]{"avi"}, true),
    WMV(new String[]{"wmv"}, true),
    THREEGP(new String[]{"3gp"}, true),
    ;

    public String[] exts;
    private boolean isVideo;

    SupportedFileType(String[] strings, boolean isVideo) {
        exts = strings;
        this.isVideo = isVideo;
    }

    public static SupportedFileType getType(String s) {
        for (SupportedFileType t : SupportedFileType.values()) {
            for (String possibility : t.exts) {
                if (s.toLowerCase().equals(possibility)) {
                    return t;
                }
            }

        }
        return null;
    }

    public String getProperExtension() {
        return exts[0];
    }

    public boolean isVideo() {
        return isVideo;
    }
}
