package brokenjpg;

import controller.IntUpdateDisplayable;
import controller.Stoppable;
import controller.StringUpdateDisplayable;
import enums.SupportedFileType;
import misc.BetterFile;
import misc.bl;

import java.util.LinkedList;
import java.util.List;

public class JpgValidityLogic extends Stoppable {

    public static final String UPDATER_SEPAROTOR = "%PATH-IS-LEFT-REASON-ON-THE-RIGHT%";
    private final IntUpdateDisplayable processedFilesUpdater;
    private final StringUpdateDisplayable brokenFilesUpdater;
    private final BetterFile rootLocation;
    private int fileAmount;

    private static JpgValidityLogic instance;

    public static  JpgValidityLogic createInstance(BetterFile rootLocation, IntUpdateDisplayable processedFilesUpdater1, StringUpdateDisplayable brokenFilesUpdater1) {
        if (instance != null)
            instance.stop();
        instance = new JpgValidityLogic(rootLocation, processedFilesUpdater1, brokenFilesUpdater1);
        return instance;
    }

    public static JpgValidityLogic createInstance(BetterFile rootLocation) {
        if (instance != null)
            instance.stop();
        instance = new JpgValidityLogic(rootLocation,
                (i -> bl.log.info("Files processed (All Filetypes): " + i)),
                (i -> bl.log.info("Broken File: " + i.split(UPDATER_SEPAROTOR)[0] + " because: " + i.split(UPDATER_SEPAROTOR)[1]))
        );
        return instance;
    }

    private JpgValidityLogic(BetterFile rootLocation, IntUpdateDisplayable processedFilesUpdater1, StringUpdateDisplayable brokenFilesUpdater1) {
        this.rootLocation = rootLocation;
        this.fileAmount = 0;
        this.processedFilesUpdater = processedFilesUpdater1;
        this.brokenFilesUpdater = brokenFilesUpdater1;
    }

    public List<BetterFile> start() {
        stop = false;
        return checkDirectoryForBrokenJpgs(rootLocation);
    }

    private List<BetterFile> checkDirectoryForBrokenJpgs(BetterFile loc) {
        LinkedList<BetterFile> brokenFiles = new LinkedList<>();
        if (loc != null && loc.exists()) {
            BetterFile[] list = loc.listFiles();
            if (list != null) {
                for (BetterFile f : list) {
                    if (stop) {
                        processedFilesUpdater.update(-1);
                        return brokenFiles;
                    }
                    if (f.isDirectory()) {
                        brokenFiles.addAll(checkDirectoryForBrokenJpgs(f));
                    } else {
                        if (f.getFileType() != null) {
                            bl.log.fine("Checking: " + f.getPath());
                            JpgAnalysisResult result = BrokenJpgDetector.checkJpgValidity(f);
                            if (f.getFileType().equals(SupportedFileType.JPG) && result != JpgAnalysisResult.VALID) {
                                brokenFilesUpdater.update(f.getPath() + UPDATER_SEPAROTOR + result.getDisplayName());
                                brokenFiles.add(f);
                            }
                            fileAmount++;
                            if (fileAmount % 100 <= 0) {
                                processedFilesUpdater.update(fileAmount);
                            }
                        }
                    }
                }
            }
        }
        return brokenFiles;
    }
}