package controller;

import javafx.application.Application;
import javafx.stage.Stage;
import model.FileModel;
import view.OrganizerView;

public class Launcher extends Application {

	public Launcher() {
	}

	public static void main(String[] args) {
		System.out.println("First Line of Code");
		launch();
		System.out.println("Last Line of Code");
	}

	@Override
	public void start(Stage stage) throws Exception {
		new OrganizerView(stage);
	}

}
