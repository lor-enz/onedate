package controller;

import misc.BetterFile;
import model.FileModel;

import static controller.MainLogic.sort;

public class FileAnalyzeLogic extends Stoppable {

    private final BetterFile root;
    private final FileModel model;
    private final IntUpdateDisplayable updater;
    private int fileAmount;

    private static FileAnalyzeLogic instance;

    public static FileAnalyzeLogic createInstance(BetterFile root, FileModel model, IntUpdateDisplayable updater) {
        if (instance != null)
            instance.stop();
        instance = new FileAnalyzeLogic(root, model, updater);
        return instance;
    }

    private FileAnalyzeLogic(BetterFile root, FileModel model, IntUpdateDisplayable updater) {
        this.fileAmount = 0;
        this.root = root;
        this.model = model;
        this.updater = updater;
    }

    public boolean start() {
        stop = false;
        analyze(root);
        if (stop)
            return false;
        else
            return true;
    }

    private void analyze(BetterFile loc) {
        BetterFile[] list = loc.listFiles();
        if (list != null) {
            for (BetterFile f : list) {
                if (stop) {
                    updater.update(-1);
                    return;
                }
                if (f.isDirectory()) {
                    analyze(f);
                } else if (sort(f)) {
                    model.add(f);
                    updater.update(++fileAmount);
                }
            }
        }
        if (!stop) // without this check we'd overwrite the -1 update
            updater.update(fileAmount);
    }


}
