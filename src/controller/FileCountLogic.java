package controller;

import misc.BetterFile;


public class FileCountLogic extends Stoppable {

    private final IntUpdateDisplayable updater;
    private final BetterFile rootLocation;
    int fileAmount;

    private static FileCountLogic instance;

    public static FileCountLogic createInstance(BetterFile rootLocation, IntUpdateDisplayable updater) {
        if (instance != null)
            instance.stop();
        instance = new FileCountLogic(rootLocation, updater);
        return instance;
    }

    private FileCountLogic(BetterFile rootLocation, IntUpdateDisplayable updater) {
        this.fileAmount = 0;
        this.updater = updater;
        this.rootLocation = rootLocation;
    }

    public int start() {
        stop = false;
        return calcFileAmount(rootLocation);
    }

    private static boolean sort(BetterFile f) {
        return (f.getFileType() != null);
    }


    private int calcFileAmount(BetterFile loc) {
        if (loc != null && loc.exists()) {
            BetterFile[] list = loc.listFiles();
            if (list != null) {

                for (BetterFile f : list) {
                    if (stop) {
                        updater.update(-1);
                        return -1;
                    }
                    if (f.isDirectory()) {
                        // TODO recursion?
                        calcFileAmount(f);
                    } else {
                        if (sort(f)) {
                            fileAmount++;
                            if (fileAmount % 1000 <= 0) {
                                updater.update(fileAmount);
                            }
                        }
                    }
                }
            }
        }
        if (!stop) // without this check we'd overwrite the -1 update
            return fileAmount;
        else
            return -1;
    }
}


