package enums;

public enum TIMES {

	METADATA("Metadata"),
	MODIFIED("Time Modified"),
	NAME("Time from Filename");

	String name;

	TIMES(String s) {
		name = s;
	}

	public static String[] getStringArray() {
		
		String[] blub = new String[TIMES.values().length];
		for (int i = 0; i < TIMES.values().length; i++) {
			blub[i] = TIMES.values()[i].name;
		}
		return blub;
	}

	public static TIMES convertString(String s) {
		for (TIMES i : TIMES.values()) {
			if (s == i.name)
				return i;
		}
		return null;
	}

}
