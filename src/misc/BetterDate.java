package misc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BetterDate extends Date {

	public BetterDate(long unixTimeMillis) {
		super(unixTimeMillis);
	}

	public BetterDate(String date) throws ParseException {
		super(new SimpleDateFormat("yyyyMMdd_HHmmss").parse(date).getTime());
	}

	public static BetterDate makeBetter(Date date) {
		return new BetterDate(date.getTime());
	}

	public String getReadableTime() {
		return formatToString("yyyy-MM-dd HH:mm:ss");
	}

	public String formatToString(String format) {
		return new SimpleDateFormat(format).format(this.getTime());
	}

	@Override
	public String toString() {
		return formatToString("yyyy-MM-dd HH-mm-ss");
	}
}
