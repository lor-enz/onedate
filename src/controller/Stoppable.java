package controller;

public abstract class Stoppable {

    protected boolean stop = false;

    public void stop() {
        stop = true;
    }
}
