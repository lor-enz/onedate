package brokenjpg;

class ImageAnalysisResult {


    boolean image;
    boolean truncated;

    public void setImage(boolean image) {
        this.image = image;
    }

    public void setTruncated(boolean truncated) {
        this.truncated = truncated;
    }

    public boolean isBroken() {
        return (!image || truncated);
    }

    @Override
    public String toString() {
        return "image: " + image + "  truncated: " + truncated;
    }
}