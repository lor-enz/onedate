package modification;

import misc.BetterDate;
import misc.BetterFile;
import misc.bl;
import model.FileAna;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.common.RationalNumber;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;
import org.apache.commons.imaging.formats.tiff.fieldtypes.FieldType;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputField;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;


import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class ExifChangerApache {


    /** Breaks every File! Is 0 Bytes after modification
     *
     * @param jpegFile
     * @param date
     * @throws IOException
     * @throws ImageReadException
     * @throws ImageWriteException
     * @throws org.apache.commons.imaging.ImageReadException
     */
    public static void changeExifTime(final File jpegFile, BetterDate date)
            throws IOException, ImageReadException, ImageWriteException, org.apache.commons.imaging.ImageReadException {


        TiffOutputSet outputSet = null;

        // note that metadata might be null if no metadata is found.
        final ImageMetadata metadata = Imaging.getMetadata(jpegFile);
        final JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
        if (jpegMetadata != null) {
            // note that exif might be null if no Exif metadata is found.
            final TiffImageMetadata exif = jpegMetadata.getExif();

            if (null != exif) {
                // TiffImageMetadata class is immutable (read-only).
                // TiffOutputSet class represents the Exif data to write.
                //
                // Usually, we want to update existing Exif metadata by
                // changing
                // the values of a few fields, or adding a field.
                // In these cases, it is easiest to use getOutputSet() to
                // start with a "copy" of the fields read from the image.
                outputSet = exif.getOutputSet();
            }
        }

        // if file does not contain any exif metadata, we create an empty
        // set of exif metadata. Otherwise, we keep all of the other
        // existing tags.
        if (null == outputSet) {
            outputSet = new TiffOutputSet();
        }

        DateFormat formatter = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
        // Create new field
        TiffOutputField newDateCreateField1 = new TiffOutputField(
                ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL,
                FieldType.ASCII,
                formatter.format(date).length(),
                formatter.format(date).getBytes());

        RandomAccessFile randomAccessFile = new RandomAccessFile(jpegFile, "r");
        byte[] b = new byte[(int) randomAccessFile.length()];
        randomAccessFile.read(b);
        try (FileOutputStream fos = new FileOutputStream(jpegFile);
             OutputStream os = new BufferedOutputStream(fos);) {

            final TiffOutputDirectory exifDirectory = outputSet.getOrCreateExifDirectory();
            // make sure to remove old value if present (this method will
            // not fail if the tag does not exist).
            exifDirectory.removeField(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL);
            exifDirectory.add(ExifTagConstants.EXIF_TAG_DATE_TIME_ORIGINAL, formatter.format(date));

            // printTagValue(jpegMetadata, TiffConstants.TIFF_TAG_DATE_TIME);
            new ExifRewriter().updateExifMetadataLossless(jpegFile, os,
                    outputSet);
        }
    }
}

