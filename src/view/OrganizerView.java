package view;

import java.io.File;
import java.io.IOException;
import java.math.RoundingMode;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import brokenjpg.JpgValidityLogic;
import com.sun.javafx.scene.control.TableColumnComparatorBase;
import controller.FileAnalyzeLogic;
import controller.FileCountLogic;
import controller.MainLogic;
import controller.MainLogic.ThreadType;
import enums.TIMES;
import enums.UPDATES;
import enums.resources;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import misc.BestFormatter;
import misc.BetterFile;
import misc.bl;
import model.FileAna;

public class OrganizerView extends Application implements Initializable, Observer {

    private MainLogic logic;

    private Stage stage;
    private long lastFPSUpdate = 0;
    private int lastFC;
    private long lastProgressUpdate = 0;

    @FXML
    private TabPane tabPane;
    @FXML
    private Tab tabSetup;
    @FXML
    private Tab tabConfirmation;
    @FXML
    private Tab tabValidity;

    @FXML
    private Button browseButton; // value will be injected by the FXMLLoader
    @FXML
    private TextField browseTextField;
    @FXML
    private Label fileNumberLabel;
    @FXML
    private CheckBox loadThumbs;
    @FXML
    private Button analyzeButton;
    @FXML
    private Button jpgValidityButton;
    @FXML
    private ProgressIndicator getFileAmountLoad;
    // Confirmation Tab
    // Table
    @FXML
    private TableView<FileAna> tableView;
    @FXML
    private TableColumn<FileAna, String> colImage;
    @FXML
    private TableColumn<FileAna, String> colName;
    @FXML
    private TableColumn<FileAna, String> colTimeName;
    @FXML
    private TableColumn<FileAna, String> colTimeModified;
    @FXML
    private TableColumn<FileAna, String> colTimeMeta;
    @FXML
    private TableColumn<FileAna, String> colPath;
    @FXML
    private TableColumn<FileAna, String> colQuality;
    // Settings in Confirmation Tab
    @FXML
    private ChoiceBox<String> timeSourceChoiceBoxOne;
    @FXML
    private ChoiceBox<String> timeSourceChoiceBoxTwo;
    @FXML
    private CheckBox checkBoxFilename;
    @FXML
    private CheckBox checkBoxTimeModified;
    @FXML
    private CheckBox checkBoxMetadata;
    @FXML
    private TextField filenameFormatTextfield;
    @FXML
    private ChoiceBox<Integer> choiceBoxParentCharNumber;
    @FXML
    private CheckBox checkBoxParentName;
    @FXML
    private Button formatHelpButton;
    @FXML
    private Button applyChangesButton;
    @FXML
    private ProgressBar progressConfirmation;
    @FXML
    private Label taskLabel;
    @FXML
    private Label percentLabel;
    @FXML
    private Label filesPerSecond;
    @FXML
    private TextArea logTextArea;
    @FXML
    private HBox hackBox;
    @FXML
    private Label about;
    // @FXML
    // private Label howTo;

    /* Validity Broken Jpg */
    @FXML
    private TableView invalidityTableView;
    @FXML
    private TableColumn invalidityPathColumn;
    @FXML
    private TableColumn invalidityProblemColumn;

    DecimalFormat df = new DecimalFormat("#.#");
    private int fileAmount;

    public OrganizerView(Stage stage) {
        resetLogic();
        this.stage = stage;
        stage.setOnCloseRequest(ae -> kill());
        stage.setTitle("Photo Organizer");
        bl.log.info("Will load FXML");
        try {
            stage.getIcons().add(new Image(enums.resources.ICON.input));
            stage.setMinWidth(1920 / 2);
            stage.setMinHeight(620);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setController(this);
            // Parent root = fxmlLoader.load(resources.FXML.input);
            Parent root = fxmlLoader.load(resources.FXML.input);
            Scene scene = new Scene(root);
            stage.setTitle("Photo Organizer");
            stage.setScene(scene);
            this.stage.show();
            addViewLogHandler();
        } catch (IOException e) {
            bl.log.severe("couldn't load FXML" + e.getMessage());
            e.printStackTrace();
            stage.setMinWidth(300);
            stage.setMinHeight(150);
            stage.setScene(new Scene(new Label("couldn't load FXML\n" + e.getMessage())));
            stage.show();
        }
        df.setRoundingMode(RoundingMode.CEILING);
    }

    /**
     * Only view related Setup
     *
     * @param stage
     * @throws Exception
     */
    public void start(Stage stage) throws Exception {
    }


    private void customiseFactory(TableColumn<FileAna, String> calltypel) {

        calltypel.setCellFactory(column -> new TableCell<FileAna, String>() {
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);

                setText(empty ? "" : getItem().toString());
                setGraphic(null);

                TableRow<FileAna> currentRow = getTableRow();
                FileAna fileAna = currentRow.getItem();


                if (fileAna != null && !isEmpty()) {
                    currentRow.setStyle("-fx-background-color:" + fileAna.getUnifyScale().getHex());
                } else
                    currentRow.setStyle("");
            }
        });
    }


    private void initTable() {
        colName.setCellValueFactory(new PropertyValueFactory<FileAna, String>("fileName"));
        colImage.setCellValueFactory(new PropertyValueFactory<FileAna, String>("thumb"));
        colTimeName.setCellValueFactory(new PropertyValueFactory<FileAna, String>("timeStringName"));
        colTimeModified.setCellValueFactory(new PropertyValueFactory<FileAna, String>("timeStringModified"));
        colTimeMeta.setCellValueFactory(new PropertyValueFactory<FileAna, String>("timeStringEXIF"));
        colPath.setCellValueFactory(new PropertyValueFactory<FileAna, String>("parent"));
        colQuality.setCellValueFactory(new PropertyValueFactory<FileAna, String>("unifyScale"));

        customiseFactory(colPath);


        tableView.setItems(logic.getModel().getTableList());
        tableView.setPlaceholder(new Label("No Content in Table. You can analyze a Folder in the 'Select Folder' Tab."));

        tableView.setRowFactory(tv -> {
            TableRow<FileAna> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    FileAna fileAna = row.getItem();
                    bl.log.info("Opening File: " + fileAna.getPath());
                    new BetterFile(fileAna.getPath()).openFile();
                }
            });
            return row;
        });


    }

    /**
     * // This method is called by the FXMLLoader when initialization is complete
     *
     * @param resources
     */
    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        bl.log.info("FXML initialized");
        // initialize your logic here: all @FXML variables will have been injected
        timeSourceChoiceBoxOne.setItems(FXCollections.observableArrayList(TIMES.getStringArray()));
        ObservableList<String> o = FXCollections.observableArrayList(TIMES.getStringArray());
        o.add("None");
        timeSourceChoiceBoxTwo.setItems(o);
        timeSourceChoiceBoxTwo.setValue("None");
        getFileAmountLoad.setVisible(false);
        analyzeButton.setDisable(true);
        applyChangesButton.setDisable(true);
        setEnableUI(false);
        logTextArea.setFont(Font.font("Monospaced", 12));
        // Buttons
        browseButton.setOnAction(ae -> {
            File f = (new DirectoryChooser().showDialog(new Stage()));
            if (f != null) {
                resetLogic();
                logic.setLocation(BetterFile.makeBetter(f));
                browseTextField.setText(f.getPath());
                startFileCount(f.getPath());
            }
        });
        formatHelpButton.setOnAction(ae -> {
            Stage s = new Stage(StageStyle.UTILITY);
            TextArea textArea = new TextArea(
                    "" + "\nLetter     Date or Time Component	     Presentation	     Examples "
                            + "\nG	        Era designator               Text	             AD"
                            + "\ny	        Year	                     Year	             1996; 96"
                            + "\nM	        Month in year	             Month	             July; Jul; 07"
                            + "\nw	        Week in year	             Number	             27"
                            + "\nW	        Week in month	             Number	             2"
                            + "\nD	        Day in year                  Number	             189"
                            + "\nd	        Day in month	             Number	             10"
                            + "\nF	        Day of week in month	     Number	             2"
                            + "\nE	        Day in week                  Text	             Tuesday; Tue"
                            + "\na	        Am/pm marker	             Text	             PM"
                            + "\nH	        Hour in day (0-23)           Number	             0"
                            + "\nk	        Hour in day (1-24)           Number	             24"
                            + "\nK	        Hour in am/pm (0-11)	     Number	             0"
                            + "\nh	        Hour in am/pm (1-12)	     Number	             12"
                            + "\nm	        Minute in hour	             Number	             30"
                            + "\ns	        Second in minute             Number	             55"
                            + "\nS	        Millisecond                  Number	             978"
                            + "\nz	        Time zone                    General time zone   Pacific Standard Time; PST; GMT-08:00"
                            + "\nZ	        Time zone                    RFC 822 time zone   -0800");
            textArea.setFont(Font.font("Monospaced", 12));
            textArea.setEditable(false);
            int width = 750;
            int height = 410;

            textArea.setMinWidth(width);
            textArea.setMinHeight(height);

            VBox box = new VBox();
            box.setAlignment(Pos.CENTER);
            Button button = new Button("OK");
            button.setOnAction(aee -> {
                s.close();
            });
            box.getChildren().addAll(textArea, button);
            s.setScene(new Scene(box));
            s.setWidth(width + 20);
            s.setHeight(height + 70);
            s.setResizable(false);
            s.show();
        });

        initTable();
        ObservableList<Integer> observableList = FXCollections.observableArrayList();
        for (int i = 1; i <= 12; i++)
            observableList.add(i);
        choiceBoxParentCharNumber.setItems(observableList);
        analyzeButton.setOnAction(ae -> {
            startFileAnalyze();
        });
        jpgValidityButton.setOnAction(ae -> {
            startJpgValidityCheck();
        });
        applyChangesButton.setOnAction(ae -> {
            startFileModification();
        });

        hack();
    }


    private void resetLogic() {
        if (logic != null) {
            logic.stopSubLogics();
            logic.stopThread(ThreadType.small);
            logic.stopThread(ThreadType.medium);
        }
        logic = MainLogic.getInstance();
        logic.addObserver(this);
    }

    /**
     * for easy testing
     */
    private void hack() {
        Button b1 = new Button("Set F:" + BetterFile.slash + "code-testing" + BetterFile.slash + "lorenz-bilder");
        hackBox.getChildren().addAll(b1);
        b1.setOnAction(ae -> {
            hack2("F:" + BetterFile.slash + "code-testing" + BetterFile.slash + "lorenz-bilder");
        });
    }

    /**
     * for easy testing
     */
    private void hack2(String s) {
        BetterFile f = new BetterFile(s);
        if (!f.exists()) {
            bl.log.warning("Hack Button misconfigured. Location doesn't exist");
            return;
        }
        resetLogic();
        logic.setLocation(f);
        browseTextField.setText(f.getPath());
        startFileCount(f.getPath());
    }

    private void startFileCount(String path) {
        logic.setLocation(new BetterFile(path));
        getFileAmountLoad.setVisible(true);
        fileNumberLabel.setText("Getting amount \n of pictures...");
        analyzeButton.setDisable(true);
        new Thread(() -> {
            logic.fileCountLogic = FileCountLogic.createInstance(new BetterFile(path), i -> {
                Platform.runLater(() -> fileNumberLabel.setText("Camera files:\n " + i + "+"));
            });
            fileAmount = logic.fileCountLogic.start();

            Platform.runLater(() -> {
                taskLabel.setText("No Task");
                analyzeButton.setDisable(false);
                getFileAmountLoad.setVisible(false);
                fileNumberLabel.setText("Camera files:\n " + fileAmount);
                filesPerSecond.setText("Files per second: " + String.format("%1$-" + (18) + "s", "0,0"));
                initTable();
            });
        }).start();
    }

    private void startJpgValidityCheck() {
        tabConfirmation.setDisable(true);
        tabPane.getSelectionModel().select(tabValidity);
        taskLabel.setText("Check Jpg Validity");
        percentLabel.setText("Starting task...");
        if (logic.jpgValidityLogic != null)
            logic.jpgValidityLogic.stop();

        new Thread(() -> {
            logic.jpgValidityLogic = JpgValidityLogic.createInstance(logic.getLocation(),
                    (i -> setProgress((int) i)),
                    (i -> validityCheckUpdate(i))
            );
            logic.jpgValidityLogic.start();
            Platform.runLater(() -> {
                taskLabel.setText("No Task");
                percentLabel.setText("Finished");
                filesPerSecond.setText("Files per second: " + String.format("%1$-" + (18) + "s", "0,0"));
                applyChangesButton.setDisable(false);
                setEnableUI(true);
            });
        }).start();
    }

    public void validityCheckUpdate(String s){
        String sep = JpgValidityLogic.UPDATER_SEPAROTOR;
        bl.log.info("Broken File: " + s.split(sep)[0] + " because: " + s.split(sep)[1]);
    }

    private void startFileAnalyze() {
        applyChangesButton.setDisable(true);
        logic.getModel().clear();
        taskLabel.setText("Task: Analyzing");
        tabPane.getSelectionModel().select(tabConfirmation);
        logic.getModel().setCreateThumbs(loadThumbs.isSelected());

        if (logic.fileAnalyzeLogic != null)
            logic.fileAnalyzeLogic.stop();

        new Thread(() -> {
            logic.fileAnalyzeLogic = FileAnalyzeLogic.createInstance(logic.getLocation(), logic.getModel(), i -> {
                setProgress((int) i);
            });
            logic.fileAnalyzeLogic.start();
            Platform.runLater(() -> {
                taskLabel.setText("No Task");
                percentLabel.setText("Finished");
                filesPerSecond.setText("Files per second: " + String.format("%1$-" + (18) + "s", "0,0"));
                applyChangesButton.setDisable(false);
                setEnableUI(true);
            });
        }).start();

    }

    private void startFileModification() {
        /* Locking UI */
        setEnableUI(false);
        applyChangesButton.setDisable(true);
        taskLabel.setText("Task: Changing Dates");
        TIMES source = TIMES.convertString(timeSourceChoiceBoxOne.getValue());
        TIMES altSource = TIMES.convertString(timeSourceChoiceBoxTwo.getValue());

        if (source != null && altSource != source)
            logic.setSourceTime(source, altSource);
        else if (source != null && altSource == source) {
            animate(timeSourceChoiceBoxOne);
            animate(timeSourceChoiceBoxTwo);
            return;
        } else if (source == null) {
            animate(timeSourceChoiceBoxOne);
            return;
        } else {
            animate(timeSourceChoiceBoxOne);
            animate(timeSourceChoiceBoxTwo);
            return;
        }

        if (checkBoxParentName.isSelected() && choiceBoxParentCharNumber.getValue() == null) {
            animate(choiceBoxParentCharNumber);
            return;
        } else {
            int chars;
            if (choiceBoxParentCharNumber.getValue() == null) {
                chars = 0;
            } else {
                chars = choiceBoxParentCharNumber.getValue();
            }
            logic.setAddParentName(checkBoxParentName.isSelected(), chars);
        }

        /* setting variables */
        logic.setPattern(filenameFormatTextfield.getText());
        logic.setChangeName(checkBoxFilename.isSelected());
        logic.setChangeMetadata(checkBoxMetadata.isSelected());
        logic.setChangeModified(checkBoxTimeModified.isSelected());
        logic.changeDates();
        setEnableUI(true);
    }

    private void setEnableUI(boolean boo) {
        checkBoxFilename.setDisable(!boo);
        checkBoxMetadata.setDisable(!boo);
        checkBoxTimeModified.setDisable(!boo);
        timeSourceChoiceBoxOne.setDisable(!boo);
        timeSourceChoiceBoxTwo.setDisable(!boo);
        checkBoxParentName.setDisable(!boo);
        choiceBoxParentCharNumber.setDisable(!boo);

    }

    private void kill() {
        bl.log.info("killing...");
        if (logic != null) {
            logic.stopThread(ThreadType.small);
            logic.stopThread(ThreadType.medium);
            logic.stopThread(ThreadType.big);
            logic.stopSubLogics();

        }
        logic = null;
        // Platform.exit();
        // System.exit(0);
    }

    public void animate(Node node) {
        double a = 0.9;
        double b = 1.4;
        double c = 1.0;

        ScaleTransition tr1 = new ScaleTransition(Duration.millis(200), node);
        tr1.setFromX(a);
        tr1.setToX(b);
        tr1.setFromY(a);
        tr1.setToY(b);
        ScaleTransition tr2 = new ScaleTransition(Duration.millis(100), node);
        tr2.setFromX(b);
        tr2.setToX(c);
        tr2.setFromY(b);
        tr2.setToY(c);
        SequentialTransition sT = new SequentialTransition();
        sT.getChildren().addAll(tr1, tr2);
        sT.play();
    }

    private void addViewLogHandler() {
        Handler h = new Handler() {
            @Override
            public void close() throws SecurityException {
            }

            @Override
            public void flush() {
            }

            @Override
            public void publish(LogRecord lr) {
                Handler hi = this;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (lr.getLevel().intValue() >= hi.getLevel().intValue())
                            appendLine(BestFormatter.bestFormat(lr));
                    }
                });
            }
        };
        h.setLevel(Level.WARNING);
        bl.addHandler(h);
    }

    /**
     * Update from MainLogic Class over Observer Pattern
     */
    @Override
    public void update(Observable obs, Object obj) {
        // int = setProgress
        // long = updateCircle
        // String = appendLine
        // FileAna = add
        if (obj instanceof String) {
            appendLine((String) obj);
        } else if (obj instanceof Integer) {
            setProgress((int) obj);
        } else if (obj instanceof UPDATES) {
            UPDATES o = (UPDATES) obj;
            stateUpdate(o);
        }
    }

    private void stateUpdate(UPDATES o) {
        Platform.runLater(() -> {
            switch (o) {
                case APPLY_FINISHED:
                    taskLabel.setText("No Task");
                    percentLabel.setText("Finished");
                    filesPerSecond.setText("Files per second: " + String.format("%1$-" + (18) + "s", "0,0"));
                    setEnableUI(true);
                    applyChangesButton.setDisable(false);
                    /* workaround to update table (prob not working.) */
                    tableView.getColumns().get(0).setVisible(false);
                    tableView.getColumns().get(0).setVisible(true);
                    /*  */
                default:
                    break;

            }
        });
    }

    /**
     * append a Line to the Textarea at the bottom
     *
     * @param s
     */
    public void appendLine(String s) {
        Platform.runLater(() -> logTextArea.appendText("\n" + s));
    }

    /**
     * Set Progress of Progressbar of the analysation with is in the
     * Confirmation Pane @param i
     *
     * @param fC the amount of files already analyzed/modified
     */
    public void setProgress(int fC) {
        float percent = (float) fC / fileAmount;
        Platform.runLater(() -> {

            long diff = System.currentTimeMillis() - lastFPSUpdate;
            if ((long) diff > 1200) {
                float x = (float) ((1000f) / diff) * ((float) (fC - lastFC));
                filesPerSecond.setText("Files per second: " + String.format("%1$-" + (18) + "s", df.format(x)));
                lastFC = fC;
                lastFPSUpdate = System.currentTimeMillis();
            }
            if (System.currentTimeMillis() - lastProgressUpdate > 350 || percent >= 0.99)
                progressConfirmation.setProgress(percent);
            if (System.currentTimeMillis() - lastProgressUpdate > 750 || percent >= 0.99) {
                lastProgressUpdate = System.currentTimeMillis();
                percentLabel.setText(df.format(percent * 100) + "%");
            }
        });
    }
}
