package misc;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import modification.ExifChangerSanselan;

import enums.SupportedFileType;
import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.ImageWriteException;

public class BetterFile extends File {

    public final static String slash = File.separator;

    public BetterFile(String arg0) {
        super(arg0);
    }

    public String getExtension() {
        if (this.getName().matches(".*\\.*.")) {
            String[] split = this.getName().split("\\.");
            String ext = split[split.length - 1];
            return ext;
        } else {
            return "";
        }
    }

    public SupportedFileType getFileType() {
        return SupportedFileType.getType(getExtension());
    }

    public String getFirstName() {
        int ext = getExtension().length();
        return getName().substring(0, getName().length() - (ext + 1));
    }

    public BetterFile renameFirstName(String s) throws IOException {
        if (s.equals(this.getFirstName())) {
            bl.log.info("No rename necessary, same name");
            return this;
        }

        String blub = (getParent() + slash + s + "." + getExtension());
        try {
            this.properRename(blub);
            return new BetterFile(blub);
        } catch (IOException e) {
            bl.log.info("Failed trying to rename " + this.getPath() + " to " + blub + " due to: " + e.getMessage());
            throw e;
        }

    }

    /**
     * Renames the file. If the name already exists it adds '~ n' with n
     * counting upwards starting at 1, until it finds an available name
     *
     * @param s The new filename (no filetype or parentpath required)
     * @return
     */
    public BetterFile intelliRenameFirstName(String s) {
        try {
            return renameFirstName(s);
        } catch (IOException e) {
            return intelliRenameFirstName(s, 1);
        }

    }

    private BetterFile intelliRenameFirstName(String s, int trys) {
        final int MAX_TRIES = 10;
        if (trys > MAX_TRIES) {
            bl.log.warning("Unable to rename after " + MAX_TRIES + " tries: " + this.getName() + " inside of "
                    + this.getParent());
            return this;
        }
        try {
            return renameFirstName(s + "~" + trys);
        } catch (IOException e) {
            return intelliRenameFirstName(s, trys + 1);
        }

    }

    private void properRename(String s) throws IOException {
        if (!new File(s).exists()) {
            Files.move(this.toPath(), new File(s).toPath());
        } else {
            throw new IOException("File already exists");
        }

    }

    public BetterFile renameExtension(String s) {
        BetterFile blub = new BetterFile(getParent() + slash + getFirstName() + "." + s);
        if (this.renameTo(blub)) {
            return blub;
        } else {
            bl.log.warning("Rename failed");
            return this;
        }
    }

    public void deleteRename() {
        intelliRenameFirstName("!delete_" + getFirstName());
    }

    public BetterFile[] listFiles() {
        File[] fileList = super.listFiles();
        if (fileList != null) {
            BetterFile[] betterList = new BetterFile[fileList.length];
            for (int i = 0; i < fileList.length; i++) {
                betterList[i] = BetterFile.makeBetter(fileList[i]);
            }
            return betterList;
        } else {
            bl.log.warning("Trouble reading files at " + this.getPath());
            return null;
        }
    }

    public static BetterFile makeBetter(File f) {
        return new BetterFile(f.getPath());
    }

    public static void moveFile(BetterFile sourceFile, BetterFile destFile) {
        Path src = Paths.get(sourceFile.getPath());
        Path dst = Paths.get(destFile.getPath() + slash + sourceFile.getName());
        System.out.println("Moving File from " + src + " to " + dst);
        try {
            Files.move(src, dst);
        } catch (IOException e) {
            System.out.println("Moving failed...");
            e.printStackTrace();
        }
    }

    public void changeFileModified(BetterDate date) {
        this.setLastModified(date.getTime());
    }

    public void openFile() {
        try {
            Desktop.getDesktop().open(this);
        } catch (IOException e) {
            bl.log.info("couldn't open file");
        }
    }

    public void changeEXIFTime(BetterDate date) {
        try {
            ExifChangerSanselan.changeExifTime(this, date);
        } catch (ImageWriteException | ImageReadException | IOException e) {
            bl.log.severe("Couldn't write Exif data to File " + this.getPath() + " with exception: " + e);
        }
    }
}
