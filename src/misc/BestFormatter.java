package misc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public final class BestFormatter extends Formatter {

	private final String LINE_SEPARATOR = System.getProperty("line.separator");
	public static int pad = 150;

	@Override
	public String format(LogRecord lr) {
		return bestFormat(lr) + LINE_SEPARATOR;
	}

	/** Formatiert LogRecord zu einem gut lesbarem String
	 * 
	 * @param lr logrecord zum formatieren
	 * @return schön formatierter string */
	public static String bestFormat(LogRecord lr) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(lr.getMillis());
		DateFormat df = new SimpleDateFormat("HH:mm:ss:SSS");
		String time = df.format(cal.getTime());
		String level = String.format("%1$-" + (12) + "s", lr.getLevel().toString());
		String part1 = " " + time + " -> " + level + " " + lr.getMessage();
		String part2 = "<- " + lr.getSourceClassName() + "." + lr.getSourceMethodName();
		String s = (String.format("%1$-" + (pad) + "s", part1) + part2);
		return s;
	}

}