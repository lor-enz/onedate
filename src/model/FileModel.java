package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import misc.BetterFile;

public class FileModel {

	private ObservableList<FileAna> tableList;
	private boolean createThumbs;

	public FileModel() {
		tableList = FXCollections.observableArrayList();
	}

	public ObservableList<FileAna> getTableList() {
		return tableList;
	}

	public void clear() {
		tableList.clear();
	}

	public void add(BetterFile f) {
		tableList.add(new FileAna(f, createThumbs));
	}

	public void setCreateThumbs(boolean selected) {
		createThumbs = selected;
	}
}
