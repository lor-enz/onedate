package model;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import enums.resources;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import misc.BetterDate;
import misc.BetterFile;
import misc.DateFindException;
import misc.bl;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Short for File Analysis. An Object of this class represents the analysis of
 * one Image File. Analysis implies getting the various forms of
 * picture-taken-times.
 *
 * @author Lorenz
 */

public class FileAna {

    private ImageView thumb;
    private String fileName;
    // String fields are necessary and linked to the table view.
    // TODO: Maybe a smart getter that works with the BetterDate Objects will
    // also work?
    private String timeStringName;
    private String timeStringModified;
    private String timeStringEXIF;
    private BetterDate dateName;
    private BetterDate dateModified;
    private BetterDate dateEXIF;

    private UnifyQuality unifyScale = UnifyQuality.NOT_AT_ALL;


    /**
     * Parent Path
     */
    private String parent; // needed for the Table
    private BetterFile file;
    private static Image placeholder;
    private static Image videoPlaceholder;

    static {
        InputStream placeholderInputStream = resources.PLACEHOLDER.input;
        InputStream videoPlaceholderInputStream = resources.VIDEOPLACEHOLDER.input;
        if (placeholderInputStream != null) {
            placeholder = new Image(placeholderInputStream, 20, 20, true, false);
            videoPlaceholder = new Image(videoPlaceholderInputStream, 70, 25, true, false);
            try {
                placeholderInputStream.close();
                videoPlaceholderInputStream.close();
            } catch (IOException e) {
                bl.log.severe("Placeholder stream couldn't be closed");
            }
        } else {
            bl.log.severe("Placeholder Inputstream is null");
        }
    }

    public FileAna(BetterFile f, boolean loadThumbs) {
        setFile(f);
        setFileName(getFile().getName());
        setParent(getFile().getParent());
        bl.log.fine("Analyzing: " + getFile().getName() + " from " + f.getParent());
        if (loadThumbs)
            thumb = createThumbnail(getFile());
        else if (isVideo())
            thumb = new ImageView(videoPlaceholder);
        else
            thumb = new ImageView(placeholder);
        updateVisualInformation();
    }

    public UnifyQuality getUnifyScale() {
        unifyScale = UnifyQuality.NOT_AT_ALL;
        if (dateName == null && dateEXIF == null) {
            unifyScale = UnifyQuality.IMPOSSIBLE;
        }
        if (dateModified != null && dateEXIF != null && dateName == null) {
            if (Math.abs(dateEXIF.getTime() - dateModified.getTime()) < 8 * 1000) {
                unifyScale = UnifyQuality.GOOD_IGNORING_NAME;
            }
        }
        if (dateName != null && dateModified != null && dateEXIF != null) {
            if (dateName.getReadableTime().equals(dateModified.getReadableTime())
                    && dateModified.getReadableTime().equals(dateEXIF.getReadableTime())) {
                unifyScale = UnifyQuality.PERFECT;
            } else if (Math.abs(dateName.getTime() - dateEXIF.getTime())
                    + Math.abs(dateName.getTime() - dateModified.getTime()) < 8 * 1000) {
                // 4 seconds difference between all three total are allowed
                unifyScale = UnifyQuality.CLOSE;
            }
        }
        return unifyScale;
    }

    public boolean isVideo() {
        return getFile().getFileType().isVideo();
    }

    public void updateVisualInformation() {
        setFileName(getFile().getName());
        setParent(getFile().getParent());
        try {
            setDateEXIF(getDateFromEXIF(getFile()));
        } catch (DateFindException e2) {
            bl.log.fine("No EXIF Data at " + getFile().getPath());
            setTimeStringEXIF("nonexistent");
        }
        try {
            setDateName(getDateFromName(getFile()));
        } catch (DateFindException e3) {
            setTimeStringName("nonexistent");
        }
        setDateModified(getDateFromFileModified(getFile()));

        bl.log.finer("WITH: " + timeStringName + " _ " + timeStringModified + " _ " + timeStringEXIF + " _ " + getUnifyScale());

    }

    private ImageView createThumbnail(final BetterFile imageFile) {
        if (isVideo()) {
            return new ImageView(videoPlaceholder);
        }
        try {
            FileInputStream stream = new FileInputStream(imageFile.getAbsolutePath());
            ImageView imageView = new ImageView(new Image(stream, 70, 30, true, true));
            try {
                stream.close();
            } catch (IOException e) {
                bl.log.warning("Failed to close stream at: " + getFile().getPath());
            }
            return imageView;
        } catch (FileNotFoundException e1) {
            bl.log.severe("FileNotFoundException" + e1.getCause());
            bl.log.severe(e1.getMessage());
        }
        return new ImageView(placeholder);
    }

    public static BetterDate getDateFromName(BetterFile f) throws DateFindException {
        String name = f.getFirstName();
        // Pattern yyyyMMdd.HHmmss
        String reg1 = "[1-2][0-9]{3}[0-1][0-9][0-3][0-9].[0-2][0-9][0-5][0-9][0-5][0-9]";
        Pattern p1 = Pattern.compile(reg1);
        Matcher m1 = p1.matcher(name);
        if (m1.find()) {
            String g = m1.group();
            char sep = g.charAt(8);
            DateFormat df = new SimpleDateFormat("yyyyMMdd" + sep + "HHmmss", Locale.GERMAN);
            try {
                return BetterDate.makeBetter(df.parse(g));
            } catch (ParseException e) {
            }
        }

        // Pattern yyyy-MM-dd.HH-mm-ss
        String reg2 = "[1-2][0-9]{3}.[0-1][0-9].[0-3][0-9].[0-2][0-9].[0-5][0-9].[0-5][0-9]";
        Pattern p2 = Pattern.compile(reg2);
        Matcher m2 = p2.matcher(name);
        if (m2.find()) {
            String g = m2.group();
            char sepLeft = g.charAt(4);
            char sepMiddle = g.charAt(10);
            char sepRight = g.charAt(13);

            DateFormat df = new SimpleDateFormat(
                    "yyyy" + sepLeft + "MM" + sepLeft + "dd" + sepMiddle + "HH" + sepRight + "mm" + sepRight + "ss", Locale.GERMAN);
            try {
                return BetterDate.makeBetter(df.parse(g));
            } catch (ParseException e) {

            }
        }

        // Pattern yyyyMMddHHmmss
        String reg3 = "[1-2][0-9]{3}[0-1][0-9][0-3][0-9][0-2][0-9][0-5][0-9][0-5][0-9]";
        Pattern p3 = Pattern.compile(reg3);
        Matcher m3 = p3.matcher(name);
        if (m3.find()) {
            String g = m3.group();
            DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss", Locale.GERMAN);
            try {
                return BetterDate.makeBetter(df.parse(g));
            } catch (ParseException e) {
            }
        }

        throw new DateFindException("No date in Name");
    }

    public BetterDate getDateFromEXIF() throws DateFindException {
        return FileAna.getDateFromEXIF(getFile());
    }

    /**
     * Grabs the time the picture was taken from the EXIF data
     *
     * @param f
     * @return Betterdate with the EXIF time the picture was taken
     * @throws DateFindException
     */
    public static BetterDate getDateFromEXIF(BetterFile f) throws DateFindException {
        if (f.getFileType().isVideo()) {
            throw new DateFindException("Video Files not fully supported yet. Sorry!");
        }
        try {
            Metadata m = ImageMetadataReader.readMetadata(f);
            ExifSubIFDDirectory directory = m.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
            if (directory != null) {
                Date pureDate = directory.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL); // time
                // zones
                // gets
                // ignored
                if (pureDate == null) {
                    throw new DateFindException("No EXIF-Time");
                }
                BetterDate date = BetterDate.makeBetter(DateToCalendar(pureDate).getTime()); // repaires
                // that
                // :D
                if (date != null) {
                    return BetterDate.makeBetter(date);
                }
            }
        } catch (ImageProcessingException e) {
            bl.log.severe("ImageProcessingException at " + f.getPath() + " WITH " + e.getMessage());
        } catch (IOException e) {
            bl.log.severe("IOException: at " + f.getPath() + " WITH " + e.getMessage());
        }

        throw new DateFindException("No EXIF-Time");
    }

    public static BetterDate getDateFromFileModified(BetterFile f) {
        return new BetterDate(f.lastModified());
    }

    @SuppressWarnings("deprecation")
    private static Calendar DateToCalendar(Date date) throws DateFindException {
        // int hours = date.getHours();
        // int timezoneOffset = date.getTimezoneOffset() / 60;
        // hours -= timezoneOffset;
        if (date != null) {

            date.setHours(date.getHours() + date.getTimezoneOffset() / 60);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.setTimeZone(TimeZone.getTimeZone("CEST"));
            // cal.set(Calendar.HOUR_OF_DAY, hours);
            return cal;
        } else
            throw new DateFindException("Got a null date in DateToCalendar");
    }

    public ImageView getThumb() {
        return thumb;
    }

    public String getFileName() {
        return fileName;
    }

    public BetterDate getDateName() {
        return dateName;
    }

    public BetterDate getDateModified() {
        return dateModified;
    }

    public BetterDate getDateEXIF() {
        return dateEXIF;
    }

    public String getTimeStringName() {
        return timeStringName;
    }

    public String getTimeStringEXIF() {
        return timeStringEXIF;
    }

    public String getTimeStringModified() {
        return timeStringModified;
    }

    public String getParent() {
        return parent;
    }

    public String getPath() {
        return getParent() + "\\" + getFileName();
    }

    public BetterFile getFile() {
        return file;
    }

    public void setFile(BetterFile file) {
        this.file = file;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public void setTimeStringName(String timeStringName) {
        this.timeStringName = timeStringName;
    }

    public void setTimeStringModified(String timeStringModified) {
        this.timeStringModified = timeStringModified;
    }

    public void setTimeStringEXIF(String timeStringEXIF) {
        this.timeStringEXIF = timeStringEXIF;
    }

    public void setDateModified(BetterDate dateModified) {
        setTimeStringModified(dateModified.getReadableTime());
        this.dateModified = dateModified;
    }

    public void setDateEXIF(BetterDate dateEXIF) {
        setTimeStringEXIF(dateEXIF.getReadableTime());
        this.dateEXIF = dateEXIF;
    }

    public void setDateName(BetterDate dateName) {
        setTimeStringName(dateName.getReadableTime());
        this.dateName = dateName;
    }

    public String toString() {
        return getFile().getPath();
    }

}
