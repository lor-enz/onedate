package tests;

import misc.BetterDate;
import misc.BetterFile;
import misc.DateFindException;
import misc.bl;
import model.FileAna;

public class ExifChangeTest {

    static final String path = "F:/code-testing/tests-dont-modify/";

    public static void changeExifFromName(String fileName) {
        FileAna fileAna = new FileAna(new BetterFile(path + fileName), false);
        BetterDate dateName = fileAna.getDateName();
        bl.log.info(" ------> " + dateName.getReadableTime());
        fileAna.getFile().changeEXIFTime(dateName);
        try {
            bl.log.info("New date is: " + fileAna.getDateFromEXIF().getReadableTime());
        } catch (DateFindException e) {
            bl.log.info("Failed to get the EXIF Data from testfile");
            e.printStackTrace();
        }
    }

    public static void changeExifaBitIntoFuture(String fileName) {
        FileAna fileAna = new FileAna(new BetterFile(path + fileName), false);
        BetterDate dateEXIF = fileAna.getDateEXIF();
        BetterDate newDateEXIF = new BetterDate(dateEXIF.getTime() + 1000*61);
        bl.log.info("OldDate: " + dateEXIF.getReadableTime());
        bl.log.info(" ------> " + newDateEXIF.getReadableTime());
        fileAna.getFile().changeEXIFTime(newDateEXIF);
        try {
            bl.log.info("New date is: " + fileAna.getDateFromEXIF().getReadableTime());
        } catch (DateFindException e) {
            bl.log.info("Failed to get the EXIF Data from testfile");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
//        changeExifaBitIntoFuture("exiftest.jpg");
        changeExifFromName("20191111_101010.jpg");
    }


}
