package controller;

public interface IntUpdateDisplayable {

    void update(long i);
}
