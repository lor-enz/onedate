package misc;

import java.io.File;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Filter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * bl steht für Burgis Logger. Bietet eine komfortable Möglichkeit alles zu loggen Der Klassenname ist absichtlich kurz gehalten und
 * möglichst kurze logaufrufe schreiben zu können.
 *
 * @author Lorenz
 */
public class bl {

    public static Level logLevel;
    public static final String jsonError;
    public static final String folderName = "log-photo-org";
    private static Filter filter;
    public static final Logger log;

    /** Konstruktor für eine statische Klasse, tolle Sache! Logger mit dazugehörigen Filter, Textdateiausgabe usw wird
     * initialisiert. */
    static {
        logLevel = Level.INFO;
        jsonError = "Aufbaufehler eines JSONObjects: ";
        log = initLogger();
        log.addHandler(startFileHandler(0));
        System.out.println("Logger created");
        bl.log.info("Logger created");
    }

    public static Logger initLogger() {
        /** Logger erstellen */
        Logger newLogger = Logger.getLogger("PhotoLogger");
        newLogger.setLevel(Level.ALL);

        /** Console Output entfernen */
        Logger rootLogger = Logger.getLogger("");
        Handler[] handlers = rootLogger.getHandlers();
        if (handlers[0] instanceof ConsoleHandler) {
            // rootLogger.removeHandler(handlers[0]);
            handlers[0].setFormatter(new BestFormatter());
        }

        return newLogger;
    }

    /**
     * configures FileHandler. Which creates itself a new logfile for every start. logfiles are numbered.
     *
     * @param i
     * @return
     */
    private static FileHandler startFileHandler(int i) {
        FileHandler fileHandler = null;
        File logFile = null;
        try {
            File dir = new File(folderName);
            dir.mkdir();
            logFile = new File(folderName + "/odLog" + i + ".txt");
            if (logFile.exists()) {
                return startFileHandler(++i);
            }
            fileHandler = new FileHandler(logFile.getPath());
            fileHandler.setLevel(Level.ALL);
            fileHandler.setFormatter(new BestFormatter());
        } catch (AccessDeniedException e) {
            System.out.println("LogFile number: " + i + " unaccessable.");
            return startFileHandler(++i);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        bl.log.info("Logfile: " + logFile.getPath() + " created");
        return fileHandler;
    }

    /**
     * Severe loggen mit Exception
     *
     * @param msg
     * @param e
     */
    public static void severe(String msg, Exception e) {
        log.log(Level.SEVERE, msg, e);
    }

    /**
     * Add Handler to logger, Globale filter influcence given handler during runtime.
     *
     * @param h
     */
    public static void addHandler(Handler h) {
        log.addHandler(h);
    }

}
