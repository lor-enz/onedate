package brokenjpg;

import controller.FileCountLogic;
import misc.BetterFile;

import java.io.File;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        desktopGoodBadTest();
        loop();
    }

    private static void loop() {
        BetterFile rootLocation = new BetterFile("F:\\code-testing\\brokenjpgtest\\");
        JpgValidityLogic loop = JpgValidityLogic.createInstance(rootLocation);
        FileCountLogic fileCountLogic = FileCountLogic.createInstance(rootLocation,
                i -> System.out.println("files counted so far: " + i + "+"));
        int totalFileAmount = fileCountLogic.start();
        System.out.println("total file amount: " + totalFileAmount);

        List<BetterFile> brokenFiles = loop.start();
        System.out.println("Broken Files: " + brokenFiles.size() + " :");
        for (BetterFile f : brokenFiles) {
            System.out.println(f.getPath());
        }

    }

    private static void desktopGoodBadTest() {
        File goodFile = new File("F:\\code-testing\\brokenjpgtest\\20170312_163649_PANO.jpg");
        File badFile = new File("F:\\code-testing\\brokenjpgtest\\broken-pano.jpg");

        System.out.print("GoodFile: ");
        System.out.println(BrokenJpgDetector.checkJpgValidity(goodFile).getDisplayName());
        System.out.print("BadFile:  ");
        System.out.println(BrokenJpgDetector.checkJpgValidity(badFile).getDisplayName());
    }
}
