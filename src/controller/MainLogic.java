package controller;

import java.text.SimpleDateFormat;
import java.util.Observable;

import brokenjpg.JpgValidityLogic;
import enums.TIMES;
import enums.UPDATES;
import misc.BetterDate;
import misc.BetterFile;
import misc.bl;
import model.FileAna;
import model.FileModel;
import model.UnifyQuality;

public class MainLogic extends Observable {

    public FileCountLogic fileCountLogic;
    public FileAnalyzeLogic fileAnalyzeLogic;
    public JpgValidityLogic jpgValidityLogic;
    public FileModifyLogic fileModifyLogic;


    private BetterFile location;

    private int fileCounter = 0;

    public FileModel getModel() {
        return model;
    }

    public void stopSubLogics() {
        if (fileCountLogic != null)
            fileCountLogic.stop();
        if (fileAnalyzeLogic != null)
            fileAnalyzeLogic.stop();
        if (fileModifyLogic != null)
            fileModifyLogic.stop();
    }

    public enum ThreadType {
        small, medium, big
    }

    /**
     * Thread that counts files
     */
    private Thread smallThread;
    /**
     * Thread that analyzes files
     */
    private Thread mediumThread;
    /**
     * Thread that renames files
     */
    private Thread bigThread;
    private Thread thisThread;
    private FileModel model;

    private String pattern;
    private TIMES sourceTime;
    private TIMES altSourceTime;
    private boolean changeMetadata = false;
    private boolean changeModified = false;
    private boolean changeName = false;
    private boolean parentName = false;
    private int parentCharNumber;

    private static MainLogic instance;

    public static MainLogic getInstance() {
        if (instance == null)
            instance = new MainLogic();
        return instance;
    }

    private MainLogic() {
        this.model = new FileModel();
        bl.log.info("MainLogic created");

    }

    /**
     * Goes over all Dates in the model table
     */
    public void changeDates() {
        fileCounter = 0;
        bigThread = new Thread() {
            public void run() {
                thisThread = Thread.currentThread();
                for (FileAna i : model.getTableList()) {
                    smartChangeDate(i, sourceTime);
                    fileCounter++;
                    update(fileCounter);
                    if (bigThread != thisThread || bigThread.isInterrupted()) {
                        bl.log.severe("BigThread cancelled");
                        return;
                    }
                }
                update(UPDATES.APPLY_FINISHED);

            }
        };
        bigThread.start();

    }

    /**
     * Changes the date according to the settings
     *
     * @param f
     * @param source
     */
    private void smartChangeDate(FileAna f, TIMES source) {
        if (f.getUnifyScale() == UnifyQuality.PERFECT) {
            // Dont't mess with the "green" rows. Because they're already perfect :)
            return;
        }

        BetterDate date;
        switch (source) {
            case METADATA:
                date = f.getDateEXIF();
                break;
            case MODIFIED:
                date = f.getDateModified();
                break;
            case NAME:
                date = f.getDateName();
                break;
            default:
                bl.log.info("No SourceTime specified :O");
                return;
        }
        if (date == null) {
            if (source == altSourceTime) {
                String s = "Neither primary nor alternative time source are available at File: "
                        + f.getFile().getPath();
                update(s);
                bl.log.info(s);
            } else {
                bl.log.fine("Primary time source isn't available at file: " + f.getFile().getPath());
                if (altSourceTime != null) {
                    smartChangeDate(f, altSourceTime);
                }
            }
            return;
        }
        if (changeMetadata && sourceTime != TIMES.METADATA) {
            bl.log.info("changing metadata on... " + f.getFile().getPath());
            f.getFile().changeEXIFTime(date);
        }

        if (changeModified && sourceTime != TIMES.MODIFIED) {
            bl.log.info("changing modified on... " + f.getFile().getPath());
            f.getFile().changeFileModified(date);
        }

        if (changeName) {
            SimpleDateFormat sdf = new SimpleDateFormat();
            sdf.applyPattern(pattern);
            String s = sdf.format(date);
            if (parentName) {
                String[] split = f.getParent().split("\\\\");
                String folderName = split[split.length - 1];
                s = s.concat(folderName.substring(0, Math.min(parentCharNumber, folderName.length())));
            }
            // f.getFile().renameFirstName(s);
            bl.log.info("changing name on....... " + padRight(f.getFile().getPath(), 60) + " to " + s);
            f.setFile(f.getFile().intelliRenameFirstName(s));
            f.setFileName(f.getFileName());
        }
    }

    public static boolean sort(BetterFile f) {
        return (f.getFileType() != null);
    }

    public void interruptThread(ThreadType type) {
        Thread thread;
        switch (type) {
            case big:
                thread = bigThread;
                break;
            case medium:
                thread = mediumThread;
                break;
            case small:
                thread = smallThread;
                break;
            default:
                return;
        }
        if (thread != null)
            thread.interrupt();
    }

    public void stopThread(ThreadType type) {
        Thread thread;
        switch (type) {
            case big:
                thread = bigThread;
                break;
            case medium:
                thread = mediumThread;
                break;
            case small:
                thread = smallThread;
                break;
            default:
                return;
        }
        if (thread != null)
            thread.interrupt();
        thread = null;
    }

    public BetterFile getLocation() {
        return location;
    }

    public void setLocation(BetterFile location) {
        this.location = location;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public void setChangeMetadata(boolean changeMetadata) {
        this.changeMetadata = changeMetadata;
    }

    public void setChangeModified(boolean changeModified) {
        this.changeModified = changeModified;
    }

    public void setChangeName(boolean changeName) {
        this.changeName = changeName;
    }

    public void setAddParentName(boolean parentName, int parentCharNumber) {
        this.parentName = parentName;
        this.parentCharNumber = parentCharNumber;

    }

    public void setSourceTime(TIMES sourceTime, TIMES altSourceTime) {
        this.sourceTime = sourceTime;
        this.altSourceTime = altSourceTime;
    }

    /**
     * update ProgressBar
     *
     * @param o
     */
    private void update(int o) {
        setChanged();
        notifyObservers(o);
    }

    private void update(long o) {
        setChanged();
        notifyObservers(o);
    }

    /**
     * Update LogArea
     *
     * @param o
     */
    private void update(String o) {
        setChanged();
        notifyObservers(o);
    }

    /**
     * various updates
     *
     * @param o
     */
    private void update(UPDATES o) {
        setChanged();
        notifyObservers(o);
    }

    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

}
