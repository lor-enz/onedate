package enums;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

public enum resources {

	FXML("res/main.fxml"),
	ICON("res/icon.png"),
	PLACEHOLDER("res/ph.png"),
	VIDEOPLACEHOLDER("res/vph.png"),
	;


	public String pathString;
	public InputStream input;
	public URI uri;
	/* broken in jar */
	public String path;

	/** Konstruktor.
	 * 
	 * @param src String */
	private resources(String src) {
		input = resources.class.getResourceAsStream(src);
		pathString = this.getClass().getResource(src).toExternalForm();
		uri = null;
		path = this.getClass().getResource(src).getFile();
		try {
			uri = this.getClass().getResource(src).toURI();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
}