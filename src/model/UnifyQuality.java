package model;

public enum UnifyQuality {

    PERFECT("cee8a7", "1 Perfect"),
    CLOSE("e4fcc0", "2 Almost perfect"),
    GOOD_IGNORING_NAME("b0f7f5", "3 Only name differs"),
    NOT_AT_ALL("d3d3d3", "4 Different"),
    IMPOSSIBLE("ffb689", "5 Only unreliable source"),

    ;


    private final String hex;
    private final String name;

    UnifyQuality(String hex, String name) {
        this.hex = hex;
        this.name = name;
    }

    public String getHex() {
        return hex;
    }

    @Override
    public String toString() {
        return name;
    }
}
