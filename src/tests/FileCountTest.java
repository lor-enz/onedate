package tests;

import controller.FileCountLogic;
import controller.IntUpdateDisplayable;
import misc.BetterFile;

public class FileCountTest {


    public static void basic() {
        BetterFile betterFile = new BetterFile("F:\\code-testing\\lorenz-bilder");

        IntUpdateDisplayable updater = new IntUpdateDisplayable() {
            @Override
            public void update(long i) {
                System.out.println(i + "+");
            }
        };

        FileCountLogic analyzer = FileCountLogic.createInstance(betterFile, updater);
        System.out.println(analyzer.start());
    }

    public static void main(String[] args) {
        basic();
    }

}
