package misc;

public class DateFindException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DateFindException() {
		super();
	}

	public DateFindException(String message) {
		super(message);
	}

	public DateFindException(String message, Throwable cause) {
		super(message, cause);
	}

	public DateFindException(Throwable cause) {
		super(cause);
	}
}
