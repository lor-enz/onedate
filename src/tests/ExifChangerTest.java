package tests;

import misc.BetterDate;
import misc.BetterFile;
import misc.DateFindException;
import misc.bl;
import model.FileAna;
import modification.ExifChangerApache;
import modification.ExifChangerSanselan;
import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.ImageWriteException;


import java.io.IOException;

public class ExifChangerTest {


    static final String path = "F:/code-testing/tests-dont-modify/";

    public static void basic(String fileName) {
        BetterFile file = new BetterFile(path + fileName);
        FileAna fileAna = new FileAna(file, false);
        BetterDate dateEXIF = fileAna.getDateEXIF();
        BetterDate newDateEXIF;
        if (dateEXIF != null) {
            newDateEXIF = new BetterDate(dateEXIF.getTime() + 1000 * 61);
            bl.log.info("OldDate: " + dateEXIF.getReadableTime());
        } else {
            newDateEXIF = new BetterDate(1570484492496L);
        }
        bl.log.info("Newdate: " + newDateEXIF.getReadableTime());

        try {
            ExifChangerSanselan.changeExifTime(file, newDateEXIF);
        } catch (IOException | ImageReadException | ImageWriteException e) {
            bl.log.severe("Couldn't write Exif data to File " + file.getPath() + " with Exception: " + e);
        }

        try {
            bl.log.info("New date is: " + fileAna.getDateFromEXIF().getReadableTime());
        } catch (DateFindException e) {
            bl.log.info("Failed to get the EXIF Data from testfile");
        }
    }

    public static void main(String[] args) {
        basic("exiftest2.jpg");
//        basic("exiftest.jpg");
    }

}