package brokenjpg;

public enum JpgAnalysisResult {
    VALID("Valid"),
    NON_IMAGE("Non Image"),
    TRUNCATED("Truncated"),
    NON_IMAGE_AND_TRUNCATED("Non Image & Truncated"),
    UNKNOWN_ERROR("Unknown Error"),
    ;

    private final String displayName;

    JpgAnalysisResult(String s) {
        this.displayName = s;
    }

    public String getDisplayName() {
        return displayName;
    }

}