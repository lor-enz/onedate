package modification;

import misc.BetterDate;

import misc.BetterFile;
import misc.bl;
import model.FileAna;
import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.ImageWriteException;
import org.apache.sanselan.Sanselan;
import org.apache.sanselan.common.IImageMetadata;
import org.apache.sanselan.formats.jpeg.JpegImageMetadata;
import org.apache.sanselan.formats.jpeg.exifRewrite.ExifRewriter;
import org.apache.sanselan.formats.tiff.constants.TiffConstants;
import org.apache.sanselan.formats.tiff.write.TiffOutputDirectory;
import org.apache.sanselan.formats.tiff.write.TiffOutputField;
import org.apache.sanselan.formats.tiff.write.TiffOutputSet;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class ExifChangerSanselan {

    /**
     * @param file
     * @param date
     */
    public static void changeExifTime(BetterFile file, BetterDate date) throws ImageWriteException, ImageReadException, IOException {
        BetterDate prevModi = FileAna.getDateFromFileModified(file);
        IImageMetadata metadata = null;
        try {

            metadata = Sanselan.getMetadata(file);

            if (metadata != null) {
                JpegImageMetadata jpegMeta = (JpegImageMetadata) metadata;
                TiffOutputSet outPutSet = jpegMeta.getExif().getOutputSet();
                // Convert String in Date Object
                DateFormat formatter = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");
                // Create new field
                TiffOutputField newDateCreateField1 = new TiffOutputField(TiffConstants.EXIF_TAG_CREATE_DATE,
                        TiffConstants.FIELD_TYPE_ASCII, formatter.format(date).length(),
                        formatter.format(date).getBytes());
                TiffOutputField newDateCreateField2 = new TiffOutputField(TiffConstants.EXIF_TAG_DATE_TIME_ORIGINAL,
                        TiffConstants.FIELD_TYPE_ASCII, formatter.format(date).length(),
                        formatter.format(date).getBytes());

                // Outputset, because getExif() results in read-only
                if (null == outPutSet) {
                    outPutSet = new TiffOutputSet();
                }
                TiffOutputDirectory exifDirectory = outPutSet.getOrCreateExifDirectory();
                exifDirectory.removeField(TiffConstants.EXIF_TAG_CREATE_DATE);
                exifDirectory.removeField(TiffConstants.EXIF_TAG_DATE_TIME_ORIGINAL);

                exifDirectory.add(newDateCreateField1);
                exifDirectory.add(newDateCreateField2);

                RandomAccessFile f = new RandomAccessFile(file, "r");
                byte[] b = new byte[(int) f.length()];
                f.read(b);
                OutputStream os = new FileOutputStream(file);
                new ExifRewriter().updateExifMetadataLossless(b, os, outPutSet);
                os.close();
                f.close();

            } else {
                bl.log.severe("Couldn't write Exif data to File " + file.getPath() + " because metadata is null");
            }
        } catch (ImageReadException | ImageWriteException | IOException e) {
            throw e;
            // bl.log.severe("Couldn't write Exif data to File " + file.getPath() + " with exception: " + e);
        } finally {
            if (prevModi != null)
                // changing exif changes file modifed. Undoing that here....
                file.changeFileModified(prevModi);
        }
    }


}

